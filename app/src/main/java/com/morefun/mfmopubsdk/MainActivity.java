package com.morefun.mfmopubsdk;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.RewardedAdListener;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;
import com.mopub.common.MoPubReward;
import com.mopub.common.SdkInitializationListener;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedAdListener;
import com.morefun.mfmopub.MFMoPub;
import com.morefun.mfmopub.MFMoPubRewardedAdListener;
import com.qp.dpk.R;

import org.w3c.dom.Text;

import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final String UNIT_ID = "87785f8534684d6aaf40306f051b3193";
    private TextView textView;
    private RewardedVideoAd rewardedVideoAd;
    private TextView fbMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MFMoPub.onCreate(this);
        MFMoPub.init(this, UNIT_ID, listener);
        textView = findViewById(R.id.message);
        fbMessage = findViewById(R.id.fb_message);
        rewardedVideoAd = new RewardedVideoAd(this, "1326021867564466_2000389623461017");
        rewardedVideoAd.loadAd(rewardedVideoAd.buildLoadAdConfig()
                .withAdListener(rewardedAdListener)
                .build());
    }

    private RewardedVideoAdListener rewardedAdListener = new RewardedVideoAdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {
            alertFb("FBonError: " + adError.getErrorMessage());
        }

        @Override
        public void onAdLoaded(Ad ad) {
            alertFb("onAdLoaded: FB");
        }

        @Override
        public void onAdClicked(Ad ad) {
            alertFb("onAdClicked: FB");
        }

        @Override
        public void onLoggingImpression(Ad ad) {
            alertFb("onLoggingImpression: FB");
        }

        @Override
        public void onRewardedVideoCompleted() {
            alertFb("onRewardedVideoCompleted: FB");
        }

        @Override
        public void onRewardedVideoClosed() {
            alertFb("onRewardedVideoClosed: FB");
        }


    };

    private MFMoPubRewardedAdListener listener = new MFMoPubRewardedAdListener() {
        @Override
        public void onRewardedAdLoadSuccess(String s) {
            alert("onRewardedAdLoadSuccess:" + s);
        }

        @Override
        public void onRewardedAdLoadFailure(String s, MoPubErrorCode moPubErrorCode) {
            alert("onRewardedAdLoadFailure:" + moPubErrorCode.toString());
        }

        @Override
        public void onRewardedAdStarted(String s) {
            alert("onRewardedAdStarted:" + s);
        }

        @Override
        public void onRewardedAdShowError(String s, MoPubErrorCode moPubErrorCode) {
            alert("onRewardedAdShowError:" + moPubErrorCode.toString());
        }

        @Override
        public void onRewardedAdClicked(String s) {
            alert("onRewardedAdClicked" + s);
        }

        @Override
        public void onRewardedAdClosed(String s) {
            alert("onRewardedAdClosed" + s);
        }

        @Override
        public void onRewardedAdCompleted(Set<String> set, MoPubReward moPubReward) {
            alert("onRewardedAdCompleted");
        }
    };

    private void alert(String text){
        textView.setText(text);
    }

    private void alertFb(String text){
        fbMessage.setText(text);
    }

    public void showFBAds(View view){
        rewardedVideoAd.show();
    }

    public void showAds(View view){
        MFMoPub.showAds();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MFMoPub.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MFMoPub.onResume(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MFMoPub.onStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MFMoPub.onDestroy(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MFMoPub.onStart(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        MFMoPub.onReStart(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MFMoPub.onBackPressed(this);
    }
}
