package com.morefun.mfmopub;

import com.mopub.common.MoPubReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedAdListener;

import java.util.Set;

public interface MFMoPubRewardedAdListener {
    void onRewardedAdClicked(String s);

    void onRewardedAdClosed(String s);


    void onRewardedAdCompleted(Set<String> set, MoPubReward moPubReward);


    void onRewardedAdLoadFailure(String s, MoPubErrorCode moPubErrorCode);


    void onRewardedAdLoadSuccess(String s);


    void onRewardedAdShowError(String s, MoPubErrorCode moPubErrorCode);


    void onRewardedAdStarted(String s);
}
