package com.morefun.mfmopub;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import com.mopub.common.MoPub;
import com.mopub.common.MoPubReward;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.FacebookAdapterConfiguration;
import com.mopub.mobileads.GooglePlayServicesRewardedVideo;
import com.mopub.mobileads.IronSourceAdapterConfiguration;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedAdListener;
import com.mopub.mobileads.MoPubRewardedAds;
import com.mopub.mobileads.PangleAdapterConfiguration;
import com.mopub.mobileads.TapjoyRewardedVideo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MFMoPub {
    private static final String TAG = MFMoPub.class.getSimpleName();
    private static final int BASE_TIME = 60 * 1000;
    private static String mMoPubAdUnitId = "";
    private final static String MOPUB_TEST_UNIT_ID = "920b6145fb1546cf8b5cf2ac34638bb7";
    public static MFMoPubRewardedAdListener mfMoPubRewardedAdListener;

    @SuppressLint("HandlerLeak")
    private static Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            loadAds();
        }
    };

    public static void init(@NonNull Context context, @NonNull String moPubAdUnitId, @NonNull MFMoPubRewardedAdListener listener) {
        mfMoPubRewardedAdListener = listener;
        mMoPubAdUnitId = moPubAdUnitId;
        if (mMoPubAdUnitId.isEmpty()){
            throw new RuntimeException("MoPubAdUnitId is empty");
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            //facebook
            Map<String, String> facebookNetworkConfig = new HashMap<>();
//            facebookNetworkConfig.put("rewarded_video", "true");
            //IS
            Map<String, String> ironSourceConfig = new HashMap<>();
            String ironSourceKey = applicationInfo.metaData.getString("com.ironsource.APPLICATION_KEY");

            if (ironSourceKey != null) {
                ironSourceConfig.put("applicationKey", ironSourceKey);
            }
            //pangle
            Map<String, String> pangleConfig = new HashMap<>();
            int pangleAppId = applicationInfo.metaData.getInt("com.pangle.APP_ID");
            if (pangleAppId != 0){
                pangleConfig.put("app_id", String.valueOf(pangleAppId));
            }
            PangleAdapterConfiguration.setRewardName("USD");


            SdkConfiguration sdkConfiguration = new SdkConfiguration.Builder(mMoPubAdUnitId)
                    .withMediationSettings(new GooglePlayServicesRewardedVideo.GooglePlayServicesMediationSettings())
                    .withMediationSettings(new TapjoyRewardedVideo.TapjoyMediationSettings())
                    .withAdditionalNetwork(PangleAdapterConfiguration.class.getName())
                    .withMediatedNetworkConfiguration(PangleAdapterConfiguration.class.getName(), pangleConfig)
                    .withMediatedNetworkConfiguration(FacebookAdapterConfiguration.class.getName(), facebookNetworkConfig)
                    .withMediatedNetworkConfiguration(IronSourceAdapterConfiguration.class.getName(), ironSourceConfig)
                    .withLogLevel(MoPubLog.LogLevel.DEBUG)
                    .withLegitimateInterestAllowed(false)
                    .build();

            MoPub.initializeSdk(context, sdkConfiguration, initSdkListener);
            MoPubRewardedAds.setRewardedAdListener(moPubRewardedAdListener);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static MoPubRewardedAdListener moPubRewardedAdListener = new MoPubRewardedAdListener() {
        @Override
        public void onRewardedAdLoadSuccess(String s) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdLoadSuccess(s);
            Log.i(TAG, "RewardedAdLoadSuccess");
        }

        @Override
        public void onRewardedAdLoadFailure(String s, MoPubErrorCode moPubErrorCode) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdLoadFailure(s, moPubErrorCode);
            Log.e(TAG, "RewardedAdLoadFailure" + moPubErrorCode.toString());
            handler.removeMessages(0);
            Log.i(TAG, "Request ads delay " + BASE_TIME / 1000 + " second ");
            handler.sendEmptyMessageDelayed(0, BASE_TIME );
        }

        @Override
        public void onRewardedAdStarted(String s) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdStarted(s);
            Log.i(TAG, "RewardedAdStarted");
        }

        @Override
        public void onRewardedAdShowError(String s, MoPubErrorCode moPubErrorCode) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdShowError(s, moPubErrorCode);
            Log.i(TAG, "RewardedAdShowError:" + moPubErrorCode.toString());
        }

        @Override
        public void onRewardedAdClicked(String s) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdClicked(s);
            Log.i(TAG, "RewardedAdClicked");
        }

        @Override
        public void onRewardedAdClosed(String s) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdClosed(s);
            Log.i(TAG, "RewardedAdClosed");
            loadAds();
        }

        @Override
        public void onRewardedAdCompleted(Set<String> set, MoPubReward moPubReward) {
            if (mfMoPubRewardedAdListener != null) mfMoPubRewardedAdListener.onRewardedAdCompleted(set, moPubReward);
        }
    };

    private static SdkInitializationListener initSdkListener = () -> {
        Log.i(TAG, "MoPubSdk init complete");
        loadAds();
    };

    private static void loadAds(){
        MoPubRewardedAds.loadRewardedAd(mMoPubAdUnitId);
        Log.i(TAG, "start load ads");
    }

    public static void showAds(){
        Log.i(TAG, "Show ads");
        MoPubRewardedAds.showRewardedAd(mMoPubAdUnitId);
    }

    public static void onCreate(Activity activity){
        MoPub.onCreate(activity);
    }

    public static void onPause(Activity activity){
        MoPub.onPause(activity);
    }

    public static void onResume(Activity activity){
        MoPub.onResume(activity);
    }

    public static void onStop(Activity activity){
        MoPub.onStop(activity);
    }

    public static void onStart(Activity activity){
        MoPub.onStart(activity);
    }

    public static void onReStart(Activity activity){
        MoPub.onRestart(activity);
    }

    public static void onDestroy(Activity activity){
        MoPub.onDestroy(activity);
    }

    public static void onBackPressed(Activity activity){
        MoPub.onBackPressed(activity);
    }
}
